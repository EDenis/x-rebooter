#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QListWidgetItem>

#include "general.h"
#include "search.h"
#include "parsing.h"
#include "reboot.h"

#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>

#include <QDebug>

list * cfg_paths;
cfg_file * current_file;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnWidth(0, 432);
    ui->tableWidget->setColumnWidth(1, 150);
    ui->tableWidget->setColumnWidth(2, 150);
    cfg_paths = NULL;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QMessageBox msgBox;
    QTableWidgetItem * itm;
    cfg_paths = cfg_files_search(FULL_SEARCH);
    boot_entry *tm;
    list *tl;
    cfg_file* tf;
    int i, j, a;
    char c;


    ui->tableWidget->setRowCount(0);
    if (cfg_paths != NULL) {
      //  while (1 == 1) {
            //printf("Founded:\n");
            i = 1;
            tl = cfg_paths;
            while (tl != NULL) {
                ui->tableWidget->setRowCount(ui->tableWidget->rowCount() + 1);

                tf = (cfg_file *)get_next_item(&tl);

                //printf("[%d] ", i);
                //printf("%s on ", tf->path);
                itm = new QTableWidgetItem(QString(tf->path));
                ui->tableWidget->setItem(i - 1, 0, itm);


                //printf("%s Booter type: ", tf->disk);
                itm = new QTableWidgetItem(QString(tf->disk));
                ui->tableWidget->setItem(i - 1, 1, itm);


                switch (tf->type) {
                    case GRUB:
                       // printf("Grub\n");
                        itm = new QTableWidgetItem("Grub");
                        ui->tableWidget->setItem(i - 1, 2, itm);
                    break;
                    case GRUB2:
                        //printf("Grub 2\n");
                        itm = new QTableWidgetItem("Grub 2");
                        ui->tableWidget->setItem(i - 1, 2, itm);
                    break;
                    case SYSLINUX:
                        //printf("Syslinux\n");
                        itm = new QTableWidgetItem("Syslinux");
                        ui->tableWidget->setItem(i - 1, 2, itm);
                    break;
                    case UNKNOWN:
                        //printf("Unknown\n");
                        itm = new QTableWidgetItem("Unknown");
                        ui->tableWidget->setItem(i - 1, 2, itm);
                    break;
                }
                i++;
            }






/*
            scanf("%d", &j);
            if (j == 0)
                return;


            tl = cfg_paths;
            for(i = 1; i <= j; i++) {
                tf = (cfg_file *)get_next_item(&tl);
            }

            //tf->disk /mnt
           // /mnt/ + tf->path
            if (a_mount(tf->disk, "/mnt") != 0) {
                printf("Mounting if failed!");
                continue;
            }


            strcpy(PATH, "/mnt");
            strcat(PATH, tf->path);
            switch (tf->type) {
                case GRUB:
                    tf->items = get_entries_grubcfg(PATH, &(tf->count));
                break;
                case GRUB2:
                    tf->items = get_entries_grub2cfg(PATH, &(tf->count));
                break;
                case SYSLINUX:
                    tf->items = get_entries_syslcfg(PATH, &(tf->count));
                break;
                case UNKNOWN:
                    printf("%s\n", MSG_UNKNOWN_BOOTER);
                    umount("/mnt");
                    continue;
                break;
            }



            printf("You have chosen %s\n", tf->path);
            if (tf->count == 0) {
                printf("%s\n", MSG_NO_BOOT_ENTRIES_INFO);
                umount("/mnt");
                continue;
            }
            print_boot_entries(tf->items, tf->count);
            printf("%s\n", MSG_CHOICE_ENTRY);
            scanf("%d", &j);
            if (j == 0) {
                umount("/mnt");
                continue;
            }
            tl = tf->items;
            for(i = 1; i <= (tf->count - j + 1); i++) {
                tm = (boot_entry *)get_next_item(&tl);
            }
            printf("You have chosen:\n");
            printf("%s      [%d]\n", MSG_SEPARATOR, j);
            printf("%s\n", tm->name);
            a = 1;
            if (tm->kernel == NULL) {
                printf("%s\n", MSG_KERINF_N_F);
                a = 0;
            } else
                printf("Kernel: %s\n", tm->kernel);
            if (tm->parameters == NULL)
                printf("%s\n", MSG_PARINF_N_F);
            else
                printf("With parameters: %s\n", tm->parameters);
            if (tm->initrd == NULL) {
                printf("%s\n", MSG_INITRDINF_N_F);
                a = 0;
            } else
                printf("Initrd: %s\n", tm->initrd);
            if (a == 1) {
                printf("Do you want to reboot? (y/n)\n");
                scanf("%c", &c);
                scanf("%c", &c);
                if (c == 'y') {
                    reboot(*tm);
                    umount("/mnt");
                    return;
                }
            } else {
                printf("%s\n", MSG_N_E_INFO);
                umount("/mnt");
            }
        }*/
    } else {
        msgBox.setText("Configuration files not found");
        msgBox.exec();
    }
    ui->tableWidget->resizeRowsToContents();

/*

    msgBox.setText("Hello Here");
    msgBox.exec();
    */
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{

}

void MainWindow::on_listWidget_clicked(const QModelIndex &index)
{

}

void MainWindow::on_listWidget_itemChanged(QListWidgetItem *item)
{

}

void MainWindow::on_listWidget_currentRowChanged(int currentRow)
{
    QMessageBox msgBox;

    QString str = QString::number(currentRow);



    msgBox.setText(str);
    msgBox.exec();
}

const char TMP_MPOINT[] = "/mnt/xrebooter-temp";
const char OUTFILE_PATH[] = "/mnt/xrebooter-temp/reboot-conf";

void MainWindow::on_pushButton_2_clicked() //перезагрузка
{
    QMessageBox msgBox;
    boot_entry * tm;
    list * tl;
    int i, j;

    j = ui->tableWidget_2->currentRow() + 1;

    tl = current_file->items;
    for(i = 1; i <= (current_file->count - j + 1); i++) {
        tm = (boot_entry *)get_next_item(&tl);
    }

    // Тип иконки сообщения
    msgBox.setIcon(QMessageBox::Information);
    // Основное сообщение Message Box
    msgBox.setInformativeText("You chose: " + QString(tm->name));
    // Добавление реагирования на софт клавиши
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    // На какой кнопке фокусироваться по умолчанию
    msgBox.setDefaultButton(QMessageBox::Ok);
    /* Запускаем QMessageBox. После выполнения, в ret будет лежать значение кнопки, на которую нажали - это необходимо для дальнейшей обработки событий*/
    int ret = msgBox.exec();
    // Собственно вот этот case и отвечает за обработку событий
    if (ret == QMessageBox::Ok) {
        //reboot(*tm);
        qDebug() << tm->disk;
        qDebug() << tm->kernel;
        qDebug() << tm->initrd;
        qDebug() << tm->parameters;

        mkdir(TMP_MPOINT, 0777);
        qDebug() << a_mount(tm->disk, TMP_MPOINT);

        FILE * outfile = fopen(OUTFILE_PATH, "w");

        fprintf(outfile, "%s\n", tm->kernel);
        fprintf(outfile, "%s\n", tm->initrd);
        fprintf(outfile, "%s\n", tm->parameters);

        fclose(outfile);
        if (0 != umount2(TMP_MPOINT, MNT_DETACH)) {
            perror("Error when unmounting: ");
        }
     }
}

void MainWindow::on_tableWidget_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
    int i, j;
    cfg_file * tf;
    char PATH[4096];
    boot_entry *mp;
    QTableWidgetItem * itm;
    list * tl;
    QMessageBox msgBox;
    //ui->tableWidget->selectRow(currentRow);

    if (cfg_paths == NULL)
        return;

    j  = currentRow + 1;
    tl = cfg_paths;
    for(i = 1; i <= j; i++) {
        tf = (cfg_file *)get_next_item(&tl);
    }


    //tf->disk /mnt
   // /mnt/ + tf->path
    //tf = (cfg_file *)tl->inf;
    if (tf->count == -1) {

        /*
        if (a_mount(tf->disk, "/mnt") != 0) {
            //printf("Mounting if failed!");
            msgBox.setText("Mounting if failed!");
            msgBox.exec();
            ui->tableWidget_2->setRowCount(0);
            return;
        }
        */

        switch (tf->type) {
            case GRUB:
                get_entries_grubcfg(tf);
            break;
            case GRUB2:
                get_entries_grub2cfg(tf);
            break;
            case SYSLINUX:
                get_entries_syslcfg(tf);
            break;
            case UNKNOWN:
                msgBox.setText("Unknown booter!");
                msgBox.exec();

                tf->count = 0;
                ui->tableWidget_2->setRowCount(0);
                return;
            break;
        }


        //umount("/mnt");
    }




    //msgBox.setText("be::: " + QString::number(tf->count));
    //msgBox.exec();


    ui->tableWidget_2->setRowCount(tf->count);
    if (tf->count == 0) {
        msgBox.setText("There is no information about boot entries in this file");
        msgBox.exec();
        //umount("/mnt");
        return;
    }

    current_file = tf;

    //print_boot_entries(tf->items, tf->count);


    mp = (boot_entry *)tf->items->inf;
    tl = tf->items;

    //msgBox.setText("p1");
    //msgBox.exec();

    for(i = (tf->count); i >= 1; i--) {

        //msgBox.setText(QString::number(i) + "g1");
        //msgBox.exec();

        if (mp->name == NULL)
            itm = new QTableWidgetItem("Not found");
        else
            itm = new QTableWidgetItem(mp->name);
        ui->tableWidget_2->setItem(i - 1, 0, itm);


        if (mp->kernel == NULL)
            itm = new QTableWidgetItem("Not found");
        else
            itm = new QTableWidgetItem(mp->kernel);
        ui->tableWidget_2->setItem(i - 1, 1, itm);

        //msgBox.setText(QString::number(i) + "g2");
        //msgBox.exec();

        if (mp->parameters == NULL)
            itm = new QTableWidgetItem("Not found");
        else
            itm = new QTableWidgetItem(mp->parameters);
        ui->tableWidget_2->setItem(i - 1, 2, itm);

        //msgBox.setText(QString::number(i) + "g3");
        //msgBox.exec();

        if (mp->initrd == NULL)
            itm = new QTableWidgetItem("Not found");
        else
            itm = new QTableWidgetItem(mp->initrd);
        ui->tableWidget_2->setItem(i - 1, 3, itm);

        //msgBox.setText(QString::number(i) + "g4");
        //msgBox.exec();

        tl = tl->next;
        if (tl != NULL)
            mp = (boot_entry *)tl->inf;

        //msgBox.setText(QString::number(i) + "g5");
        //msgBox.exec();
    }

    ui->tableWidget_2->resizeRowsToContents();
}
