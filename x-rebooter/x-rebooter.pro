#-------------------------------------------------
#
# Project created by QtCreator 2016-07-28T19:09:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = x-rebooter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    libs/general.h \
    libs/search.h

FORMS    += mainwindow.ui





win32:CONFIG(release, debug|release): LIBS += -L$$PWD/libs/release/ -lfast_reboot
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/libs/debug/ -lfast_reboot
else:unix: LIBS += -L$$PWD/libs/ -lfast_reboot

INCLUDEPATH += $$PWD/libs
DEPENDPATH += $$PWD/libs
