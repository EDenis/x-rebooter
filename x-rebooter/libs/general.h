extern const char* MSG_ERR_CFGFILE_N_F;
extern const char* MSG_ERR_ANALYSIS_FILE_ERROR;
extern const char* MSG_KERINF_N_F;
extern const char* MSG_PARINF_N_F;
extern const char* MSG_INITRDINF_N_F;
extern const char* MSG_SEPARATOR;
extern const char* MSG_CHOICE_FILE;
extern const char* MSG_CHOICE_ENTRY;
extern const char* MSG_N_E_INFO;
extern const char* MSG_START_SCAN;
extern const char* MSG_NO_BOOT_ENTRIES_INFO;
extern const char* MSG_UNKNOWN_BOOTER;

extern const int LEN_MOUNT;

enum search_type {DEFAULT_PATHS_ONLY, FULL_SEARCH};
enum cfg_file_type {GRUB, GRUB2, SYSLINUX, UNKNOWN};

typedef struct List
{
	void *inf;
	struct List *next;
} list;

// Структура с информацией о варианте загрузки
typedef struct Boot_entry
{
    char *name, *kernel, *parameters, *initrd, *disk;
} boot_entry;

// Структура с описанием конфигурационного файла
typedef struct Cfg_file
{
    char *path, *disk; // Путь до файла и диск, на котором он находится
    list *items; // Список вариантов загрузки, представленных в файле
                 // NULL - если анализ файла ещё не производился
    int count;  //count - количество вариантов загрузки, представленных в файле
                // -1 - если анализ файла ещё не производился
    enum cfg_file_type type;//type - тип загрузчика (Grub, Syslinux)
} cfg_file;

list * add_to_list(list *st, void *inf);

void * get_next_item(list **ptr);

void free_list(list *l);

void choice (list* cfg_paths);
