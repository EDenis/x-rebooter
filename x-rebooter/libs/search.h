list * get_disks_list();

int a_mount(const char *dev, const char *dir);

void search(char *st_fr, char *target, list **res);

void grub_search(list **res);

void grub2_search(list **res);

void sysl_search(list **res);

list * cfg_files_search(enum search_type T);
