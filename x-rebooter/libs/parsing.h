void get_entries_grubcfg(cfg_file *cfg_f);

void get_entries_grub2cfg(cfg_file *cfg_f);

void get_entries_syslcfg(cfg_file *cfg_f);

void print_boot_entries(list *p, int k);
